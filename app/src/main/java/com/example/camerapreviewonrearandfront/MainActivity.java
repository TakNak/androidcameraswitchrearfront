package com.example.camerapreviewonrearandfront;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.media.ImageReader;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.media.ImageReader.newInstance;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "CameraPreview";
    private TextureView textureView;
    CameraDevice mCameraDevice;
    private CameraCaptureSession mCaptureSession;
    static int cnt = 0;
    private Surface surface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textureView = findViewById(R.id.textureView);
        textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                ImageReader imageReader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 2);

                openCamera();
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {

            }
        });

        textureView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeCamera();
                openCamera();
            }
        });
    }
    private void closeCamera() {
        if(mCaptureSession == null) {
            return;
        }
        try {
            mCaptureSession.abortCaptures();
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        try {
            mCaptureSession.stopRepeating();
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        mCaptureSession.close();
        mCaptureSession = null;

        mCameraDevice.close();
        mCameraDevice = null;
    }

    private void openCamera() {
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);

        try {
            String[] cameraId = manager.getCameraIdList();
            int num = cameraId.length;
            cnt++;
            Log.d(TAG, "num:" + num + ", cnt:" + cnt + ", cnt%num:" + cnt%num);

            int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

            if(permission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions( new String[] {Manifest.permission.CAMERA}, 1);
                return;
            }

            manager.openCamera(cameraId[cnt%num], stateCallback, null);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

    }

    private CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            Log.d(TAG, "onOpend");
            mCameraDevice = camera;
            try {
                createCameraPreviewSession();
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            Log.d(TAG, "onDisconnected");
            if(mCameraDevice != null) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            Log.d(TAG, "onError");
            onDisconnected(camera);
            finish();
        }
    };

    private CameraCaptureSession.CaptureCallback mCaptureCallback = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
            super.onCaptureCompleted(session, request, result);

            closeCamera();
            Log.d(TAG, "onCaptureCompleted");
        }
    };

    private void createCameraPreviewSession() throws CameraAccessException {
        SurfaceTexture texture = textureView.getSurfaceTexture();
        texture.setDefaultBufferSize(textureView.getWidth(), textureView.getHeight());
        surface = new Surface(texture);

        mCameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
            @Override
            public void onConfigured(@NonNull CameraCaptureSession session) {
                if(mCameraDevice == null) {
                    return;
                }

                if(false){
                    mCaptureSession = session;

                    CaptureRequest.Builder previewRequestBuilder = null;
                    try {
                        previewRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                    previewRequestBuilder.addTarget(surface);

                    previewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                            CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                    CaptureRequest previewRequest = previewRequestBuilder.build();

                    try {
                        mCaptureSession.setRepeatingRequest(previewRequest, null, null);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }
                {
                    mCaptureSession = session;

                    CaptureRequest.Builder captureRequrest = null;
                    try {
                        captureRequrest = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                    captureRequrest.addTarget(surface);

                    //ArrayList<CaptureRequest> requestList = new ArrayList<CaptureRequest>();
                    captureRequrest.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                    //captureRequrest.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION, 6);
                    //captureRequrest.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION, 0);
//                    /captureRequrest.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION, 6);
                    //requestList.add(captureRequrest.build());

                    try {
                        mCaptureSession.capture(captureRequrest.build(), mCaptureCallback, null);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onConfigureFailed(@NonNull CameraCaptureSession session) {

            }
        }, null);


    }

}
